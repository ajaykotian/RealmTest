package com.example.ajaykotian.realmtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import java.util.List;

import io.realm.Realm;
import io.realm.Sort;


public class MainActivity extends AppCompatActivity {

    protected EditText editText;
    protected Button submit;
    protected Button delete;
    protected TextView textView;

    protected Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        realm = Realm.getDefaultInstance();


        editText = (EditText) findViewById(R.id.et_main);
        textView = (TextView) findViewById(R.id.tv_result);
        submit = (Button) findViewById(R.id.button_submit);
        delete = (Button) findViewById(R.id.button_delete);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String text = editText.getText().toString();

                if (!text.isEmpty()) {
                    Number currentMaxId = realm.where(TableObject.class).max("id");
                    int nextid;
                    if (currentMaxId == null) {
                        nextid = 0;
                    } else {
                        nextid = currentMaxId.intValue() + 1;
                    }

                    TableObject tableObject = new TableObject();
                    tableObject.setId(nextid);
                    tableObject.setVal(Integer.parseInt(text));
                    realm.beginTransaction();

                    realm.copyToRealm(tableObject);
                    realm.commitTransaction();
                }

                showData();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    TableObject tableObject = realm.where(TableObject.class).findAllSorted("id", Sort.DESCENDING).first();
                    Boolean flag = (tableObject == null);
                    Log.e("Value", flag.toString());
                    if (tableObject != null) {
                        realm.beginTransaction();
                        tableObject.deleteFromRealm();
                        realm.commitTransaction();
                    }
                } catch (Exception e) {

                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("Exception:",e.getMessage());
                }
                showData();
            }
        });

    }

    protected void showData() {
        List tabledata = realm.where(TableObject.class).findAll();
        String output = "";

        for (int i = 0; i < tabledata.size(); i++) {
            TableObject temp = (TableObject) tabledata.get(i);
            output += "Id:  " + temp.getId() + "  Val:  " + temp.getVal() + "\n";
        }

        textView.setText(output);
    }


}
