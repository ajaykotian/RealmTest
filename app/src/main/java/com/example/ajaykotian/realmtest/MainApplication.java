package com.example.ajaykotian.realmtest;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by ajaykotian on 14/7/17.
 */

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

       Realm.init(this);
    }
}
