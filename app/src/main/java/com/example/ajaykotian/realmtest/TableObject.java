package com.example.ajaykotian.realmtest;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ajaykotian on 14/7/17.
 */

public class TableObject extends RealmObject {

    @PrimaryKey
    private int id;

    private int val;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }
}
